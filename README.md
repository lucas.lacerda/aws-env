aws-env - Secure way to handle environment variables in Docker
------------------------

**aws-env** is a small utility that tries to solve problem of passing environment variables to applications in a secure way, especially in a Docker containers. It uses [AWS Parameter Store](https://aws.amazon.com/ec2/systems-manager/parameter-store/) to securely store applications' configuration -- ideal for storing all kind of secrets.